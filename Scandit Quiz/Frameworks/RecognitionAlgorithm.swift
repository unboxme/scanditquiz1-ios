//
//  RecognitionAlgorithm.swift
//  Scandit Quiz
//
//  Created by Pavel Puzyrev on 23/10/2018.
//  Copyright © 2018 Pavel Puzyrev. All rights reserved.
//

/// Should to be stored in Framework or in Pods project.

import UIKit

//// Holds all settings for the RecognitionAlgorithm. Intentionally left empty.
public struct Settings { /* ... */ }

/// Holds the result of the RecognitionAlgorithm. Intentionally left empty.
public struct Result { /* ... */ }

/// The amazing RecognitionAlgorithm
public class RecognitionAlgorithm {
    
    func apply(settings: Settings) { /* ... */ }

    func process(frame: UIImage) -> Result {
        let result = Result() /* ... */
        return result
    }
    
}
