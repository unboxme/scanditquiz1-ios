//
//  ViewController.swift
//  Scandit Quiz
//
//  Created by Pavel Puzyrev on 23/10/2018.
//  Copyright © 2018 Pavel Puzyrev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    /// Bad solution to store it in VC, it should to be stored in service layer
    /// Anyway it enough to quiz I suppose
    private let faceRecognizer: FaceRecognizer = Facify()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        faceRecognizer.add(listener: self)
    }
    
    deinit {
        faceRecognizer.remove(listener: self)
    }

}

extension ViewController: Listener {
    
    func faceRecognizer(_ faceRecognizer: FaceRecognizer, didProcess frame: UIImage, result: Result) {
        // Boom!
    }
    
}

