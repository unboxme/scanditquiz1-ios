//
//  Foundation+Extension.swift
//  Scandit Quiz
//
//  Created by Pavel Puzyrev on 23/10/2018.
//  Copyright © 2018 Pavel Puzyrev. All rights reserved.
//

/// Just helpers.
/// Should to be stored in Framework. One file per extension.

import Foundation

public extension OperationQueue {
    
    convenience init(maxConcurrentOperationCount: Int) {
        self.init()
        self.maxConcurrentOperationCount = maxConcurrentOperationCount
    }
    
}

public extension Array {
    
    func has(_ object: AnyObject) -> Bool {
        guard let _ = index(of: object) else {
            return true
        }
        
        return false
    }
    
    func index(of object: AnyObject) -> Int? {
        guard let index = firstIndex(where: { $0 as AnyObject === object }) else {
            return nil
        }
        
        return index
    }
    
}

public extension String {
    
    public var bundlify: String {
        return Bundle.main.bundleIdentifier! + "." + self
    }
    
}
