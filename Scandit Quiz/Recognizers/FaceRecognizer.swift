//
//  FaceRecognizer.swift
//  Scandit Quiz
//
//  Created by Pavel Puzyrev on 23/10/2018.
//  Copyright © 2018 Pavel Puzyrev. All rights reserved.
//

import UIKit

/// Protocol for a listener interested in the result of the FaceRecognizer
protocol Listener: class {
    
    func faceRecognizer(_ faceRecognizer: FaceRecognizer, didProcess frame: UIImage, result: Result)
    
}

/// The protocol that needs to be implemented
protocol FaceRecognizer: class {
    
    /// Add a listener
    func add(listener: Listener)
    
    /// Remove a listener
    func remove(listener: Listener)
    
    /// The setter applies the recognitionSettings to the RecognitionAlgorithm.
    var recognitionSettings: Settings { get set }
    
    /// Process a frame and invokes the listeners on the specified queue.
    /// - parameter frame: The frame to process.
    /// - parameter completionQueue: The queue on which the listeners are invoked.
    func process(frame: UIImage, completionQueue: DispatchQueue)
    
}
