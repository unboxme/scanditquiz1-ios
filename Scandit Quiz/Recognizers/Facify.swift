//
//  Facify.swift
//  Scandit Quiz
//
//  Created by Pavel Puzyrev on 23/10/2018.
//  Copyright © 2018 Pavel Puzyrev. All rights reserved.
//

import UIKit

/// `NSOperationQueue` with `underlyingQueue` provides flexible and readable solution for other developers
/// unlike using just only `DispatchQueue`, `DispatchGroup`, etc. Otherwise it's possible to make a batch of processing
/// operations, but according to the documentation of `RecognitionAlgorithm` basic operations may take a while, so
/// the results for listeners will be mixed – it's bad.
///
/// This class will be easily extended by chaging `maxConcurrentOperationCount` and new managing of `completionQueue`.

class Facify: FaceRecognizer {
    
    // MARK: - Public Properties
    
    // MARK: FaceRecognizer
    
    /// Any default settings from `RecognitionAlgorithm`
    var recognitionSettings = Settings()
    
    // MARK: - Private Properties
    
    private let recognitionAlgorithm = RecognitionAlgorithm()
    private var completionQueue: DispatchQueue!
    
    private let processQueue = DispatchQueue(label: "process_queue".bundlify)
    private let operationQueue = OperationQueue(maxConcurrentOperationCount: 1)
    private var listeners: [Listener] = []
    
    // MARK: - Initializer
    
    init() {
        operationQueue.underlyingQueue = processQueue
    }
    
    // MARK: - Public Methods
    
    // MARK: FaceRecognizer
    
    func add(listener: Listener) {
        if listeners.has(listener) {
            listeners.append(listener)
        }
    }
    
    func remove(listener: Listener) {
        if let index = listeners.index(of: listener) {
            listeners.remove(at: index)
        }
    }
    
    func process(frame: UIImage, completionQueue: DispatchQueue) {
        guard operationQueue.operationCount > 0 else {
            return
        }
        
        self.completionQueue = completionQueue
        
        let operation = BlockOperation {
            self.recognitionAlgorithm.apply(settings: self.recognitionSettings)
            let result = self.recognitionAlgorithm.process(frame: frame)
            
            for listener in self.listeners {
                self.completionQueue.async {
                    listener.faceRecognizer(self, didProcess: frame, result: result)
                }
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
}
